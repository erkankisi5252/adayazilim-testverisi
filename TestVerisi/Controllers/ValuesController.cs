﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.Entity;
using Dal;
using TestVerisi;

namespace TestVerisi.Controllers
{
    public class ValuesController : ApiController
    {
        Entities db = new Entities();
        Random rnd = new Random();
        // GET api/values

        [AllowAnonymous]
        [Route("api/Values/TestVerisiOlustur")]
        [HttpPost]
        public void TestVerisiOlustur(int MusteriAdet, int SepetAdet)
        {
            List<string> sehirler = new List<string>() { "Ankara", "İstanbul", "İzmir", "Bursa", "Edirne", "Konya", "Antalya", "Diyarbakır", "Van", "Rize" };

            for (int i = 0; i < MusteriAdet; i++)
            {
                Musteri Musteris = new Musteri();
                Musteris.MusteriAdi = RandomKarakter(10);
                Musteris.MusteriSoyAdi = RandomKarakter(10);
                Musteris.Sehir = sehirler[rnd.Next(0, 10)].ToString();
                db.Musteri.Add(Musteris);
                db.SaveChanges();
            }
            var SonMusteriList = db.Musteri.OrderByDescending(x => x.Id).Take(MusteriAdet).ToList();
            for (int x = 0; x < SepetAdet; x++)
            {
                Sepet spt = new Sepet();
                spt.MusteriId = SonMusteriList[rnd.Next(0, MusteriAdet)].Id;
                db.Sepet.Add(spt);
                db.SaveChanges();
                var sayi = rnd.Next(1, 6);
                for (int k = 0; k < sayi; k++)
                {
                    SepetUrun spetUrun = new SepetUrun();
                    spetUrun.Aciklama = RandomKarakter(10);
                    spetUrun.SepetId = spt.Id;
                    spetUrun.Tutar = rnd.Next(100, 1000);
                    db.SepetUrun.Add(spetUrun);
                    db.SaveChanges();
                }
            }



        }

        [AllowAnonymous]
        [Route("api/Values/SehirAnalizi")]
        [HttpGet]
        public List<Models.DtoSehirAnaliz> SehirAnalizi()
        {
            List<Models.DtoSehirAnaliz> SehirAnalizt = new List<Models.DtoSehirAnaliz>();

            var sehirler = db.Musteri.GroupBy(x => x.Sehir).ToList();
            double SepetUrunToplamTutar = 0;
            foreach (var item in sehirler)
            {
                var musteriList = db.Musteri.Where(x => x.Sehir == item.Key).ToList();
                int SepetAdet = 0;
                SepetUrunToplamTutar = 0;
                foreach (var musteri in musteriList)
                {
                    var MusteriSepetleri = db.Sepet.Where(x => x.MusteriId == musteri.Id).ToList();
                    SepetAdet += MusteriSepetleri.Count();
                    foreach (var MusteriSepeti in MusteriSepetleri)
                    {
                        SepetUrunToplamTutar += Convert.ToDouble(db.SepetUrun.Where(x => x.SepetId == MusteriSepeti.Id).Sum(y => y.Tutar));
                    }
                }

                SehirAnalizt.Add(new Models.DtoSehirAnaliz
                {
                    SehirAdi = item.Key,
                    SepetAdet = SepetAdet,
                    ToplamTutar = SepetUrunToplamTutar

                });
            }
            return SehirAnalizt;
        }
        public string RandomKarakter(int sayi)
        {
            string Text = "";
            for (int i = 0; i < sayi; i++)
            {
                Text += ((char)rnd.Next('A', 'Z')).ToString();

            }
            return Text;
        }

    }
}
