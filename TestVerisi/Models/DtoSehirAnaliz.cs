﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestVerisi.Models
{
    public class DtoSehirAnaliz
    {
        public string SehirAdi { get; set; }
        public int SepetAdet { get; set; }
        public double ToplamTutar { get; set; }
    }
}